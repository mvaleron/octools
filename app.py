#!/usr/bin/python
from app.app_factory import create_app
from waitress import serve
from secret import config




if __name__ == '__main__':
    app = create_app(config)
    serve(app, host='0.0.0.0', port=8080)
