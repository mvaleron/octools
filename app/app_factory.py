from __future__ import print_function
from importlib import import_module

import os
from flask import Flask

from app.api_loader import load_api_public_blueprints
from app.cli.cli import init_cli
from app.cli.v1.load_cli import load_cli_commands, load_cli_namespaces
from app.extensions import (db, migrate, cache, jwt, celery, csrf)
from werkzeug.middleware.proxy_fix import ProxyFix



def create_app(config_filename):
    """
    Factory to create the application using a file

    :param config_filename: The name of the file that will be used for configuration
    :return: The created application
    """
    app = Flask(__name__)
    app.config.from_object(config_filename)


    if app.config.get('USE_PROXY', False):
        app.wsgi_app = ProxyFix(app.wsgi_app)

    print('Initialize cli...', end='')
    # initialize cli, where we add additional commands to the CLI
    #init_cli(app)
    bp = load_cli_commands()
    app.register_blueprint(bp)
    #load_cli_v1(app)
    print("ok")

    """s
    Initialize Cache
    """
    init_redis_cache(app)
    """
    JWT
    """
    init_jwt_auth(app)
    """
    Loading API
    """
    print("Loading public API... ", end='')
    load_public_api_v1(app)
    print("ok")

    """
    Init CSRF
    """
    #csrf.init_app(app)

    #@app.before_first_request
    #def init_settings():
    #    print("Creating the settings object if needed... ", end='')

    """
    Global variables for templates
    """

    #@app.context_processor
    #def inject_opencast_endpoint():
    #    opencast_endpoint = app.config["OPENCAST_ENDPOINT"]
    #    is_dev = app.config["IS_DEV"]
    #    return dict(opencast_endpoint=opencast_endpoint, is_dev=is_dev)

    return app

def init_redis_cache(app):
    cache_config = {'CACHE_TYPE': 'null'}
    if app.config.get('CACHE_ENABLE', False):
        print("Initializing Redis Caching... ", end='')
        cache_config = {'CACHE_TYPE': 'redis',
                        "CACHE_REDIS_HOST": app.config.get('CACHE_REDIS_HOST', None),
                        "CACHE_REDIS_PASSWORD": app.config.get('CACHE_REDIS_PASSWORD', None),
                        "CACHE_REDIS_PORT": app.config.get('CACHE_REDIS_PORT', None)

                        }
    else:
        print("Caching disabled... ", end='')
    cache.init_app(app, config=cache_config)
    print("ok")

def load_cli_v1(application):
    all_blueprints = load_cli_namespaces()

    for bp in all_blueprints:
        application.register_blueprint(bp)

def load_public_api_v1(application):
    all_blueprints = load_api_public_blueprints()

    for bp in all_blueprints:
        application.register_blueprint(
            bp, url_prefix='{prefix}/{version}'.format(prefix='/api', version='v1'))


def init_jwt_auth(app):
    """
    Initializes the JWT Authentication and sets the global configuration for the application JWT
    :param app: Application that will be set up
    :return: -
    """
    print("Initializing JWT Authentication... ", end='')
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

    jwt.init_app(app)
    print("ok")


