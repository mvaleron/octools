
import logging

from app.api.api_authorizations import authorizations
from flask_restx import Namespace, Resource, abort, reqparse, fields
from app.daos.api_tokens import ApiTokenDAO
from app.extensions import csrf

logger = logging.getLogger('octools.tools_api')

namespace = Namespace('tools', description='Additional tools', authorizations=authorizations,
                      security=['Bearer Token'])


test_model = namespace.model('ToolsModel', {
    'result': fields.String(required=True, description='The tools result'),
})



#@namespace.doc(security=['Bearer Token'])
@namespace.doc()
@namespace.route('/token')
class TestTokenEndpoint(Resource):
    #method_decorators = [jwt_resource_required('opencast')]
    decorators = [csrf.exempt]
    @namespace.doc('tools_token ')
    @namespace.doc(params={'name': "The name of the new token"})
    @namespace.doc(params={'resource': "A valid resource for the token"})

    def get(self):
        """
        Gets a valid token for a resource.
        You should be SSO authenticated and belong to an authorized group such as opencast-admins.

        :return: A tuple with the response dict and the valid token
        :rtype: tuple[dict, int]
        """
        logger.info("Tools Token API endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, help='The name of the token')
        parser.add_argument('resource', type=str, help='The resource for the token')
        args = parser.parse_args()
        resource = args.resource
        name = args.name
        if not name or not resource:
            abort(400, "name or resource were not provided", error=True)

        try:
            model = ApiTokenDAO.create_do_no_store({ "name": name, "resource": resource})
            return {"result": model, "error": False}, 200
        except Exception as e:
            logger.error(
                "{}: Error: Tools API processing endpoint. Exception: {}".format(name,str(e)))
            abort(400, name + ":" + str(e), error=True)
