import logging
from flask import Flask, request, abort, jsonify, send_from_directory

from flask_restx import Namespace, Resource, abort, reqparse

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required
from app.services.opencast.opencast_api import OpencastAPI
import os


logger = logging.getLogger('octools.opencast_api')

namespace = Namespace('opencast', description='Opencast related operations', authorizations=authorizations,
                      security=['Bearer Token'])

MASTER_DIRECTORY='/mnt/master_share/master_data'


@namespace.doc(security=['Bearer Token'])
@namespace.route('/trs')
class OpencastTrsEndpoint(Resource):
    method_decorators = [jwt_resource_required('opencast')]

    @namespace.doc('opencast_trs')
    @namespace.doc(params={'event_id': "Event ID for the operation"})
    def get(self):
        """
        Uploads a video/audio to the transcription service for an event id.

        This endpoint will return error code 400 if the event doesnt exist in Opencast.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast API transcription endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument('event_id', type=str, help='The Event ID to be uploaded to the transcription services')
        args = parser.parse_args()
        event_id = args.event_id

        if not event_id:
            abort(400, "No event_id provided", error=True)

        try:
            logger.info("{}: Opencast API processing endpoint request".format(event_id))
            print("{}: Opencast API processing endpoint request".format(event_id))
            result = True
            if not result:
                logger.error(
                    "{}: Error: Opencast API processing endpoint request error. Unable to send the ingest.".format(event_id))
                abort(400, "Unable to upload the media file. Does the event exis?", error=True)
            return {"result": "OK", "error": False}, 200
        except Exception as e:
            #logger.error(
            #    "{}: Error: Opencast API processing endpoint. Exception: {}".format(event_id,str(e)))
            print("Unknown error processing endpoint request.")
            abort(400, "Error:" + str(e), error=True)

@namespace.doc(security=['Bearer Token'])
@namespace.route('/status')
class OpencastStatusEndpoint(Resource):
    method_decorators = [jwt_resource_required('opencast')]
    @namespace.doc('opencast_status')
    @namespace.doc(params={'event_id': "Event ID for the operation"})
    def get(self):
        """
        Make a request to the Opencast API to retrieve the status of a given event

        :return: The API response
        :rtype: requests.Response
        """
        path = "/events/"

        logger.info("Opencast API transcription endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument('event_id', type=str, help='The Event ID to be uploaded to the transcription services')
        args = parser.parse_args()
        event_id = args.event_id

        if not event_id:
            abort(400, "No event_id provided", error=True)

        try:
            event_data = OpencastAPI(event_id).get_event_status()
            print(event_data)
            # indico.py -action ingest cli command
            return  {'result': event_data['status'], 'error':False}
        except Exception as e:
            logger.error("{}: Error: Opencast Event API endpoint. (Error: {})".format(event_id, str(e)))
            print("{}: Error: Opencast Event API endpoint. (Error: {})".format(event_id, str(e)))
            abort(400, str(e), error=True)

@namespace.doc(security=['Bearer Token'])
@namespace.route('/command')
class OpencastCommandEndpoint(Resource):
    method_decorators = [jwt_resource_required('opencast')]

    @namespace.doc('opencast_command')
    @namespace.doc(params={'addargs': "Other additional args to parse"})    
    @namespace.doc(params={'event_id': "Event ID for the operation"})
    @namespace.doc(params={'command': "Command to run"})
    def get(self):
        """
        Runs a tool command.

        This endpoint will return error code 400 if command fails.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("Opencast command tool endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument('command', type=str, help='The command tool to run')
        parser.add_argument('event_id', type=str, help='The Event ID for the command')
        parser.add_argument('addargs', type=str, help='Aditional args for the command')

        args = parser.parse_args()
        event_id = args.event_id
        command = args.command
        addargs = args.addargs

        if not event_id:
            abort(400, "No event_id provided", error=True)
        if not command:
            abort(400, "No event_id provided", error=True)
        try:
            logger.info("{}: Opencast command endpoint request".format(event_id))
            print("{}: Opencast command endpoint request".format(event_id))
            cmd="indico.py -c /etc/pycast/pycast.cfg -action=%s -ev_id=%s %s"%(command, event_id, addargs)
            print(cmd)
            output = os.popen(cmd).read()
            result = False
            if (output != ''):
                if (output =="error\n"):
                    print("Error running command")
                else:
                    print("ok")  
                    print(output)     
                    result = True
  
            if not result:
                logger.error(
                    "{}: Error: Opencast processing command tool endpoint request error.".format(event_id))
                abort(400, "Unable to run the command. Does the event exis?", error=True)
            return {"result": output, "error": False}, 200
        except Exception as e:
            #logger.error(
            #    "{}: Error: Opencast API processing endpoint. Exception: {}".format(event_id,str(e)))
            print("Unknown error processing endpoint request.")
            abort(400, "Error:" + str(e), error=True)



@namespace.doc(security=['Bearer Token'])
@namespace.route('/masters/list/<path:dir>/<path:contribution_id>')
class OpencastListMasterEndpoint(Resource):
    method_decorators = [jwt_resource_required('opencast')]

    @namespace.doc('masters_list')
    def get(self, dir, contribution_id):
        """
        Lists the master files.

        This endpoint will return error code 400 if the download fails.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        files = []
        fullpath = os.path.join(MASTER_DIRECTORY, dir, contribution_id)
        print(fullpath)
        for filename in os.listdir(fullpath):
            subpath = os.path.join(fullpath, filename)
            if os.path.isfile(subpath):
                files.append(filename)
        return {"result":  files, "error": False}, 200



@namespace.doc(security=['Bearer Token'])
@namespace.route('/masters/get/<path:dir>/<path:contribution_id>/<path:file>')
class OpencastDownloadMasterEndpoint(Resource):
    method_decorators = [jwt_resource_required('opencast')]

    @namespace.doc('masters_download')
    def get(self, dir, contribution_id, file):
        """
        Downloads the master files.

        This endpoint will return error code 400 if the download fails.

        :return: A file

        """
        logger.info("Opencast download endpoint")
        fullpath = os.path.join(MASTER_DIRECTORY, dir, contribution_id)

        try:
            logger.info("{}: Opencast download master endpoint request".format(fullpath))
            print("{}: Opencast download master endpoint request".format(fullpath))      
            return send_from_directory(fullpath, file, as_attachment=True)

        except Exception as e:
                #logger.error(
                #    "{}: Error: Opencast API processing endpoint. Exception: {}".format(fullpath,str(e)))
                print("Unknown error processing endpoint request.")
                abort(400, "Error:" + str(e), error=True)            