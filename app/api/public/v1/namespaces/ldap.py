import logging
from flask import Flask, request, abort, jsonify, send_from_directory

from flask_restx import Namespace, Resource, abort, reqparse

from app.api.api_authorizations import authorizations
from app.api.decorators import jwt_resource_required
from app.services.ldap.cern_ldap_service import CernLdapService
import os


logger = logging.getLogger('octools.ldap_api')

namespace = Namespace('ldap', description='Opencast related operations', authorizations=authorizations,
                      security=['Bearer Token'])


@namespace.doc(security=['Bearer Token'])
@namespace.route('/ldap/search')
class LdapSearchEndpoint(Resource):
    #method_decorators = [jwt_resource_required('opencast')]

    @namespace.doc('ldap_search')
    @namespace.doc(params={'email': "User email for the search"})
    def get(self):
        """
        Search a user by email in the LDAP Directory.

        This endpoint will return error code 400 if the user doesnt exist or not found.

        :return: A tuple with the response dict and the response code
        :rtype: tuple[dict, int]
        """
        logger.info("LDAP API transcription endpoint")
        parser = reqparse.RequestParser()
        parser.add_argument('email', type=str, help='The User ID to search for')
        args = parser.parse_args()
        email = args.email

        if not email:
            abort(400, "No email provided", error=True)

        try:
            logger.info("{}: LDAP Search endpoint request".format(email))
            print("{}:  LDAP Search endpoint request".format(email))
            result = True
            export_allowed_list = {'groups': [], 'users': [], 'externals': []}
            not_found_list = {'groups': [], 'users': []}

            ldap_user = CernLdapService.get_user_by_email(email)
            #if not ldap_user:
            #    ldap_user = CernLdapService.get_user_by_email(email, external=True)
            #    if ldap_user:
            #        export_allowed_list['externals'].append({"uid": ldap_user['saMAccountName'], "email":email})
            if ldap_user:
                export_allowed_list['users'].append("{}".format(ldap_user['username']))
            else:
                not_found_list['users'].append(email)



            if not ldap_user:
                logger.error(
                    "{}: Error:  LDAP Search  endpoint request error. Unable to find the user.".format(email))
                abort(400, "Unable to find the user. Does the event exis?", error=True)
            return {"result": ldap_user, "error": False}, 200
        except Exception as e:
            logger.error("{}: Error: Opencast API processing endpoint. Exception: {}".format(email,str(e)))
            print("Unknown error processing endpoint request.")
            abort(400, "Error:" + str(e), error=True)