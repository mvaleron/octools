from flask import Blueprint
from flask_restx import Api

from app.api.public.v1.namespaces.opencast import namespace as oc_namespace
from app.api.public.v1.namespaces.tools import namespace as ts_namespace
from app.api.public.v1.namespaces.ldap import namespace as ld_namespace
from app.api.public.v1.namespaces.hello import namespace as ho_namespace

def load_api_namespaces():
    bp = Blueprint('public_api', 'public_api')
    api = Api(bp)
    api.add_namespace(oc_namespace)
    api.add_namespace(ts_namespace)
    api.add_namespace(ld_namespace)    
    api.add_namespace(ho_namespace)


    return bp