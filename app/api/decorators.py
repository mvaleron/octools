from flask_jwt_extended import verify_jwt_in_request, get_jwt_identity
from flask_restx import abort

from flask import jsonify

import logging

from sqlalchemy.orm.exc import NoResultFound
from app.daos.api_tokens import ApiTokenDAO


logger = logging.getLogger('webapp.require_token')


def jwt_resource_required(resource):
    """
    Custom decorator that verifies the JWT is present in
    the request, as well as insuring that this user has access to the given resource.
    This way, the access token will only work with the selected API endpoints

    :param resource: Name of the resource (opencast, tools, etc)
    :return:
    """
    def decorator(fn):
        def wrapper(*args, **kwargs):
            logger.info("Loading jwt_resource_required")
            verify_jwt_in_request()
            name = get_jwt_identity()
            try:
                #api_key = ApiTokenDAO.get_by_name(name)
                print(name)
                api_key = ApiTokenDAO.get_resource(name) #{ "name": "opencast", "resource":"opencast"} 
                print(api_key)
                print("Check resource:"+resource)
                if api_key:
                    if api_key.resource != resource:
                        abort(401, "Token not valid for resource.", error=True)
                else:
                    abort(400, "Token ot found", error=True)
            except NoResultFound:
                print("Token not found")
                abort(401, "Forbidden", error=True)
            else:
                return fn(*args, **kwargs)
        return wrapper
    return decorator
