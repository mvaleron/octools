import datetime

from flask_dance.consumer.backend.sqla import OAuthConsumerMixin
from flask_login import UserMixin

from app.extensions import db


class User(db.Model, UserMixin):
    __tablename__ = 'user'
    id = db.Column(db.Integer, unique=True, primary_key=True)
    first_name = db.Column(db.String(255))
    username = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    email = db.Column(db.String(255), unique=True)
    is_admin = db.Column(db.Boolean, nullable=False, default=False)

    department = db.Column(db.String(255), nullable=True)
    group = db.Column(db.String(255), nullable=True)
    section = db.Column(db.String(255), nullable=True)
    organization = db.Column(db.String(255), nullable=True)
    building = db.Column(db.String(255), nullable=True)

    active = db.Column(db.Boolean, default=True)
    confirmed_at = db.Column(db.DateTime, default=datetime.datetime.now)

    def __repr__(self):
        return "<User {id}: {first_name} {last_name} {username} >".format(id=self.id, first_name=self.first_name,
                                                                          last_name=self.last_name,
                                                                          username=self.username)


class OAuth(db.Model, OAuthConsumerMixin):
    """
    Represents an Oauth connection in the application
    """
    __tablename__ = 'flask_dance_oauth'

    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    user = db.relationship(User)
