from flask_sqlalchemy import SQLAlchemy
from flask_caching import Cache
from flask_migrate import Migrate
from flask_jwt_extended import JWTManager
from celery import Celery
from flask_wtf.csrf import CSRFProtect

# Initialize our database and its migrations
db = SQLAlchemy()
migrate = Migrate()
# Initialize caching
cache = Cache(config={'CACHE_TYPE': 'null'})
# Revoked token blacklist for the API
jwt = JWTManager()
# Initialize Celery
celery = Celery()
# Csrf protection for forms
csrf = CSRFProtect()
