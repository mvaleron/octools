import logging
from sqlalchemy.orm.exc import NoResultFound
from flask_jwt_extended import create_access_token
from app.extensions import db
from app.models.api_token import ApiToken
from app.utils.blacklist_helpers import add_token_to_database
from collections import namedtuple
from flask import jsonify


logger = logging.getLogger('webapp.daos_api_tokens')


class ApiTokenException(Exception):
    pass


class ApiTokenDAO:
    CACHE_TIMEOUT = 10  # seconds.

    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def get_all():
        token = ApiToken.query.all()
        return token

    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def get_by_id(token_id):
        token = ApiToken.query.get(token_id)
        return token

    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def get_by_name(token_name):
        token = ApiToken.query.filter_by(name=token_name).one()

        return token

    @staticmethod
    def create_precondition(info):
        assert info.get('name', None)
        assert info.get('resource', None)

    @staticmethod
    def create(info):
        ApiTokenDAO.create_precondition(info)
        try:
            ApiToken.query.filter_by(name=info['name'].strip().lower()).one()
        except NoResultFound:
            logger.info("Creating Token with name {}".format(info['name']))
            access_token = create_access_token(identity=info['name'])
            add_token_to_database(info['name'], access_token, info['resource'])
            return True

    @staticmethod
    def create_do_no_store(info):
        ApiTokenDAO.create_precondition(info)
        logger.info("Creating Token with name {}".format(info['name']))
        access_token = create_access_token(identity=info['name'])
        return access_token

    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def get_resource(token_name):
        TokenStruct = namedtuple('TokenStruct', 'name resource')
        try:
            if (token_name == 'opencast'):
                token = TokenStruct( name = token_name, resource = 'opencast')        
            else:
                #raise NoResultFound
                token = TokenStruct( name = token_name, resource = 'other')     
        except NoResultFound:
            print("Token not found.")
            token = None
        return token

        
    @staticmethod
    # @cache.memoize(CACHE_TIMEOUT)
    def delete_by_id(service_id):
        token = ApiToken.query.get(service_id)

        if token:
            token.revoked = True

            db.session.commit()
            logger.info(
                "Token with id {} has been revoked".format(service_id))
            return True
        else:
            logger.warning(
                "Unable to revoke the Token with id {} (Not found)".format(service_id))
        return False
