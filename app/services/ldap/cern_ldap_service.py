import logging

from app.extensions import cache
from app.services.ldap.cern_ldap_client import CernLdapClient

logger = logging.getLogger("webapp.services_ldap")

class CernLdapService:

    CACHE_TIMEOUT = 180 # 180 seconds. 3 minutes

    @staticmethod
    @cache.memoize(CACHE_TIMEOUT)
    def get_user_by_email(email, external=False):
        """
        Get a user from LDAP by it's email
        :param email: Email of the user
        :type email: str
        :return: The user
        :rtype: dict
        """
        logger.debug("Not using a cached method")
        print("Not using a cached method")
        ldap_client = CernLdapClient()
        user = ldap_client.get_user_by_email(email, external=external)
        return user

    @staticmethod
    @cache.memoize(CACHE_TIMEOUT)
    def get_user_by_username(username):
        """
        Get a user by it's username
        :param username: Username of the user
        :type username: str
        :return: The user
        :rtype: dict
        """
        logger.debug("Not using a cached method")
        ldap_client = CernLdapClient()
        user = ldap_client.get_user_by_username(username)
        return user

    @staticmethod
    @cache.memoize(CACHE_TIMEOUT)
    def get_user_by_person_id(person_id):
        """
        Get a user by it's person's ID
        :param person_id: Person's id of the user
        :type person_id: str
        :return: The user
        :rtype: dict
        """
        logger.debug("Not using a cached method")
        ldap_client = CernLdapClient()
        user = ldap_client.get_user_by_person_id(person_id)
        return user

    @staticmethod
    @cache.memoize(CACHE_TIMEOUT)
    def search_users_by_username(username):
        """
        Get a list of users by their username
        :param username: username to be searched
        :type username: str
        :return: List of users
        :rtype: list[dict]
        """
        logger.debug("Not using a cached method")
        ldap_client = CernLdapClient()
        users = ldap_client.search_users(username)
        return users


