import json
import logging

import requests
from flask import current_app
from requests_toolbelt import MultipartEncoder

from secret import config


class OpencastAPI:

    def __init__(self, event_id, logger=None):
        """

        :param event: The event involved in the operation
        :type event: UUID (String)
        :param logger:
        :type logger:
        """
        self.event_id = event_id
        self.OPENCAST_ENDPOINT = current_app.config["OPENCAST_ENDPOINT"]
        self.OPENCAST_USER = current_app.config["OPENCAST_USER"]
        self.OPENCAST_PASS = current_app.config["OPENCAST_PASSWORD"]
        self.session = requests.Session()
        self.session.auth = (self.OPENCAST_USER, self.OPENCAST_PASS)
        if logger:
            self.logger = logger
        else:
            self.logger = logging.getLogger("job.opencast_api")
        print("Opencast user: %s"%(self.OPENCAST_USER))

    def get_event_status(self):
        """
        Make a request to the Opencast API to retrieve the status of a given workflow

        :return: The API response
        :rtype: requests.Response
        """
        path = "/api/events/"

        if self.event_id is None or self.event_id == "":
            return None

        url = self.OPENCAST_ENDPOINT + path + self.event_id
        self.logger.info(
            "{}: Opencast - Sending get event status request: {}".format(self.event_id, url))

        try:
            response = self.session.get(url, timeout=5, verify=False)
            self.logger.info(
                "{}: Opencast - get event status completed: {}".format(self.event_id, url))
            return response.json()
        except Exception as ex:
            self.logger.exception(
                "{}: Opencast - Error while sending a request:\n{}\n{}".format(self.event_id,url,ex))
            raise ex
