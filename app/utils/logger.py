import logging
import sys
from datetime import datetime
from logging import handlers
from pytz import timezone, utc

from app.utils.uma_logger import UMAHandler


def zurich_time(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Europe/Zurich")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()


def setup_logs(app, logger_name, to_stdout=True, to_remote=False, to_file=False, to_mail=False):
    """
    Set up the logs for the application

    :param app: Flask application where the configuration will be obtained
    :type app: flask.Flask
    :param logger_name: The name of the logger
    :type logger_name: str
    :param to_stdout: Whether or not to log to stdout
    :type to_stdout: bool
    :param to_remote: Whether or not to use remote logging
    :type to_remote: bool
    :return:
    :rtype:
    """
    logger = logging.getLogger(logger_name)

    if app.config['LOG_LEVEL'] == 'DEV':
        logger.setLevel(logging.DEBUG)

    if app.config['LOG_LEVEL'] == 'PROD':
        logger.setLevel(logging.INFO)

    formatter = logging.Formatter(
        '%(asctime)s | %(levelname)s | %(name)s | %(message)s | %(pathname)s | %(funcName)s():%(lineno)d')

    logging.Formatter.converter = zurich_time

    if to_stdout:
        configure_stdout_logging(logger=logger, formatter=formatter, log_level=app.config['LOG_LEVEL'])

    if to_remote:
        configure_remote_logging(logger=logger, app=app)

    if to_file:
        configure_file_logging(logger=logger, app=app, formatter=formatter, file_name=logger_name)

    if to_mail:
        configure_email_logging(logger=logger, app=app)


def configure_stdout_logging(logger=None, formatter=None, log_level="DEV"):
    """
    Set up the stdout logging

    :param logger: The logger to be configured
    :type logger: logging.Logger
    :param formatter: Formatter to be used
    :type formatter: logging.Formatter
    :param log_level: The level of the logging: DEV|PROD
    :type log_level: str
    :return:
    :rtype:
    """
    stream_handler = logging.StreamHandler(stream=sys.stdout)

    stream_handler.setFormatter(formatter)
    if log_level == 'DEV':
        stream_handler.setLevel(logging.DEBUG)
    if log_level == 'PROD':
        stream_handler.setLevel(logging.INFO)

    logger.addHandler(stream_handler)
    print("Logging {logger_name} to stdout -> True".format(logger_name=str(logger)))


def configure_remote_logging(logger=None, app=None, formatter=None):
    """
    Set up the remote logging

    :param app: Flask application used to get the configuration
    :type app: flask.Flask
    :param logger: The logger to be configured
    :type logger: logging.Logger
    :param formatter: Formatter to be used
    :type formatter: logging.Formatter
    :return:
    :rtype:
    """
    if not app.config.get('LOG_REMOTE_PRODUCER', None) or not app.config.get('LOG_REMOTE_TYPE', None):
        print("ERROR -> Unable to configure the remote logging. Attributes not set.")

    handler = UMAHandler(app.config.get('LOG_REMOTE_PRODUCER', None), app.config.get('LOG_REMOTE_TYPE', None))

    handler.setFormatter(formatter)

    def handle_exception(exc_type, exc_value, exc_traceback):
        if issubclass(exc_type, KeyboardInterrupt):
            sys.__excepthook__(exc_type, exc_value, exc_traceback)
            return

        logger.error("Uncaught exception", exc_info=(exc_type, exc_value, exc_traceback))

    sys.excepthook = handle_exception
    if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
        handler.setLevel(logging.DEBUG)
    if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
        handler.setLevel(logging.INFO)

    logger.addHandler(handler)
    print("Logging {logger_name} to remote -> True".format(logger_name=str(logger)))


def configure_file_logging(logger=None, app=None, formatter=None, file_name="application"):
    log_file_name = '{log_file_path}/{name}.log'.format(
        log_file_path=app.config.get("LOG_FILE_PATH", "/opt/app-root/src/logs"), name=file_name)
    try:
        file_handler = handlers.TimedRotatingFileHandler(log_file_name,
                                                                 when='midnight', interval=7, backupCount=5)
        file_handler.setFormatter(formatter)
        if app.config.get('LOG_LEVEL', 'DEV') == 'DEV':
            file_handler.setLevel(logging.DEBUG)
        if app.config.get('LOG_LEVEL', 'DEV') == 'PROD':
            file_handler.setLevel(logging.INFO)

        logger.addHandler(file_handler)
        print("Logging {logger_name} to file ({file_name}) -> True".format(logger_name=str(logger),
                                                                           file_name=log_file_name))

    except Exception as e:
        print("It seems there is a problem with the volume. Logs will be only on stdout. Error: {}".format(e))


def configure_email_logging(logger=None, app=None):
    fmt_email = logging.Formatter("""
            Message type:  %(levelname)s
            Name:          %(name)s
            Location:      %(pathname)s:%(lineno)d
            Module:        %(module)s/%(filename)s
            Function:      %(funcName)s
            Time:          %(asctime)s
            Message:

            %(message)s
        """)

    # email in case of errors
    mail_handler = logging.handlers.SMTPHandler(app.config.get('LOG_MAIL_HOSTNAME'),
                                                app.config.get('LOG_MAIL_FROM'),
                                                app.config.get('LOG_MAIL_TO'), "[CES2] Application error")
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(fmt_email)
    logger.addHandler(mail_handler)
    print("Logging {logger_name} to email -> True".format(logger_name=str(logger)))
