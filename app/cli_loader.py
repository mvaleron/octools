from app.cli.v1.load_cli import load_cli_namespaces


def load_cli_blueprints():
    """
    Load all the blueprints, adding the development ones if needed

    :return: A tuple of Blueprint objects
    :rtype: list[flask.Blueprint]
    """
    all_blueprints = (
        load_cli_namespaces(),
    )

    return all_blueprints