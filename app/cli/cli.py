import logging

import click
import os

from app.extensions import db


def init_cli(app):
    """
    Add additional commands to the CLI. These are loaded automatically on the app.py

    :param app: App to attach the cli commands to
    :return: None
    """

    @app.cli.command()
    def test_command():
        """
        Echo a string
        """
        click.echo('Running a command')
        result = "Ok"

    @app.cli.command("check-event")
    @click.argument("event_id")
    @click.option("--publications", default=False, help='Does it exist any publication for the event?')
    def check_event(event_id: str, publications: bool):
        """
        Checks if an Opencast event exists

        :param event_id: The contribution ID of the contribution.
        :type event_id: str (unique identifier)
        :return:
        :rtype:
        """
        click.echo('Checking event {}. Publications: {}'.format(event_id, publications))

    @app.cli.command("message")
    @click.argument("message")
    def print_message(message):
        print("Log internal: %s"%(message))

    @app.cli.command("tool-command")
    @click.argument("action")
    @click.argument("args") 
    def command(action, args):
        cmd="indico.py -c /etc/pycast/pycast.cfg -action %s %s"%(action, args)
        output = os.popen(cmd).read()
        if (output != ''):
            if (output =="error\n"):
                print("Error running command")
            else:
                print("ok")  
                print(output)     

        return output        
