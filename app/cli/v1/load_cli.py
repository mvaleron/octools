from flask import Blueprint

from app.cli.v1.namespaces.opencast import opencastbp as oc_bp
from app.cli.v1.namespaces.commands import load_command_blueprint as cm_bp

def load_cli_commands():


    #commandsbp = Blueprint('test', __name__)
    #return cm_bp(commandsbp)
    return oc_bp


def load_cli_namespaces():
    return oc_bp, cm_bp