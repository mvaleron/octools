import click
from flask import Blueprint

def load_command_blueprint(commandsbp):

    @commandsbp.cli.command('create')
    @click.argument('name')
    def create(name):
        """ Creates a user """
        print("Create user: {}".format(name))

    return commandsbp