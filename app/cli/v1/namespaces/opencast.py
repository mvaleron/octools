import click
from flask import Blueprint

opencastbp = Blueprint('opencast', __name__)

@opencastbp.cli.command('check')
@click.argument('event_id')
def ckeck(event_id):
    """ Check a event """
    print("Check event: {}".format(event_id))

