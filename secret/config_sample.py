import os
CURRENT_TIMEZONE = "Europe/Zurich"
USE_PROXY = False

SECRET_KEY = '\xf1\xc4&C\x07\xc6\xbc0\x8e\xd2\xc0\xe2\xac,\xd5\x80\xd9\xe7%\x8e\xf3\x19\x1e\x93'
SESSION_PERMANENT = False
SESSION_TYPE = "filesystem"

DEBUG = True

LOG_LEVEL = "DEV"
LOG_REMOTE_ENABLED = False
LOG_REMOTE_PRODUCER = "octools"
LOG_REMOTE_TYPE = "oc-qa"
PROPAGATE_EXCEPTIONS = True

#########################
# VALID TOKENS
#########################
TOKENS = ['','']

#########################
# CACHING CONFIGURATION
#########################
CACHE_ENABLE = True
CACHE_REDIS_HOST = "redis"
CACHE_REDIS_PORT = "6379"
CACHE_REDIS_PASSWORD = "<TODO>"


################
# Opencast
################
OPENCAST_ENDPOINT="https://ocweb-test.cern.ch"
OPENCAST_USER="xxxx"
OPENCAST_PASSWORD='yyyy'
# When the Opencast resource file is downloaded, it will be store on the following folder
# In production, the expected folder is: /downloads
DOWNLOADS_FOLDER_PATH = '/opt/app-root/src/downloads'
