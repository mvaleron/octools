# /app/models.py

## previous  imports ###
import jwt
from datetime import datetime, timedelta

#class User(db.Model):
class User():
    """Maps to users table """

    __tablename__ = 'users'

    ###########################################
    ## Existing code for defining table columns is here  ##
    ###########################################

    def __init__(self, email, password):
        #### INIT CODE LIES HERE ###################
        ###########################################
        self.email = email

    def password_is_valid(self, password):
        ##### PASSWORD CHECK CODE LIES HERE ####
        ###########################################
        print("pwd valid")
    def save(self):
        ######### CODE FOR SAVING USER LIES HERE ##
        ############################################
        print("save")
    def generate_token(self, secret, user_id):
        """ Generates the access token"""

        try:
            # set up a payload with an expiration time
            payload = {
                'exp': datetime.utcnow() + timedelta(minutes=5),
                'iat': datetime.utcnow(),
                'sub': user_id
            }
            # create the byte string token using the payload and the SECRET key
            jwt_string = jwt.encode(
                payload,
                secret, #current_app.config.get('SECRET'),
                algorithm='HS256'
            )
            return jwt_string

        except Exception as e:
            # return an error in string format if an exception occurs
            return str(e)

    @staticmethod
    def decode_token(secret, token):
        """Decodes the access token from the Authorization header."""
        try:
            # try to decode the token using our SECRET variable
            payload = jwt.decode(token, secret)
            return payload['sub']
        except jwt.ExpiredSignatureError:
            # the token is expired, return an error string
            return "Expired token. Please login to get a new token"
        except jwt.InvalidTokenError:
            # the token is invalid, return an error string
            return "Invalid token. Please register or login"