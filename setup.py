from setuptools import setup,find_packages

setup(
    name='OCTools',
    url='https://gitlab.cern.ch/mvaleron/octools',
    author='Miguel Angel Valero',
    author_email='mvaleron@cern.ch',
    install_requires=['requests','configobj','flask','setuptools','argh','click','pyjwt','flask_restx','flask_jwt_extended', \
    'sqlalchemy','gunicorn','pymysql' ,'requests-toolbelt','smbprotocol','flask_sqlalchemy','redis','Werkzeug','flask_caching', \
     'flask_migrate','celery','flask_wtf','waitress', 'flask-session','ldap3'],
    version='1.0.19', 
    license='MIT',
    scripts = ['app.py', 'wsgi.py', 'tests.py'],
    description='Opencast API Rest Tools for Webcast',
    long_description='Opencast API Rest Tools for Webcast',
    long_description_content_type="text/markdown",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    entry_points={
        'flask.commands': [
            'opencast=cli.v1.namespaces.opencast:check'
        ],
    },
    include_package_data=True,
    package_data={'app': ['templates/*.html','templates/dashboard/*.html']},
)