#!/usr/bin/python3.6
from flask import Flask, session, redirect, url_for
from flask import request, render_template
from flask import Blueprint
from waitress import serve
import click
from flask.cli import AppGroup
from flask_session import Session
from test.models import User
from os import path, system
import os


app = Flask(__name__)
app.config["SECRET_KEY"] = "SECRET_KEY"
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"


opencast = AppGroup('opencast')

@opencast.command('mllp')
@click.argument('event_id')
def mllp(event_id):
    message("The click mllp command: %s"%(event_id))

app.cli.add_command(opencast)
user = User(email='opencast@cern.ch', password='0penCast')


def check_token(secret, auth_header):
    if auth_header == None:
        return {"data": "","error":"Not Authorization header found"}
    else:
        access_token = auth_header.split(" ")[1]   
        if access_token:
            # Get the user id related to this access token
            user_id = User.decode_token(secret, access_token)
            return {"data": user_id,"error":None}
        else:
            return {"data": "","error":"Not bearer provided"}

##################################################
##################################################
@app.route("/login", methods=['GET','POST'])
def login():
    session['auth']=False
    if (request.method =='GET'):
        return render_template('login.html', error="Invalid token")
    else:
        if (request.form['token']):
            access_token = request.form['token']
            if access_token is None:
                # redirect the user to our LoginRadius login URL if no access token is provided
                # return redirect(LR_AUTH_PAGE.format("login", request.base_url))
                return "You are not logged in"
            elif access_token == "b3BlbmNhc3Q6MTIzNA==":
                # fetch the user profile details with their access tokens
                session['auth']=True
                session['user']='opencast'
                print(session['user'])
                result = {"data": session['user'],"error":None}
            else:
                print("error")
                result =  {"data": "","error":'Invalid token'}
            return result

@app.route("/auth", methods=['POST'])
def auth():            
    print("check_token")
    result =  check_token(app.config["SECRET_KEY"], request.headers.get('Authorization'))
    if result.get("error") is not None:
        # redirect the user to our login URL if there was an error
        session['auth']=True
        session['user']=result.get("data")
        print(session['user'])           
        return  result.get("data") 
    else:
            
        return result         

    
@app.route("/token", methods=['GET', 'POST'])
def token():
    if request.method == 'POST':
        access_user = request.form['username']
        print(access_user)
        if access_user == 'opencast':
            token = user.generate_token(app.config["SECRET_KEY"],access_user)
            result =  {"data": token,"error":None}
        else:
            result =  {"data": "","error":'Invalid user'}
        return result
    else:
        return render_template('token.html', error="Invalid user")

@app.route("/logout")
def logout():
    session.clear()
    return redirect("/")

@app.route("/dashboard")
def dashboard():
    if 'auth' in session and session['auth'] == True:
        return "You have successfully logged in!"
    else:
        return "You are not logged in"

@app.route('/set')
def set():
    session['user'] = 'opencast'
    return 'ok'

@app.route('/get')
def get():
    if 'user' in session:
        return session.get('user', 'not set')
    else:
        return "Not defined"

##################################################
##################################################

def message(message):
    print("Log internal: %s"%(message))

def command(command, args):
    cmd="indico.py -c /etc/pycast/pycast.cfg -action %s %s"%(command, args)
    output = os.popen(cmd).read()
    if (output != ''):
        if (output =="error\n"):
            print("Error running command")
        else:
            print("ok")  
            print(output)     

    return output

@app.route('/')
def hello():
    return "Default route!"

@app.route('/opencast')
def get_opencast():
  message = "This is the Opencast Endpoint."

  return message

@app.route('/opencast/export')
def get_export():
  message = "This is the Opencast export Endpoint."

  return message

@app.route('/opencast/notify')
def get_notify():
  message = "This is the Opencast notify Endpoint."

  return message

@app.route('/opencast/master')
def get_master():
  message = "This is the Opencast master Endpoint."

  return message


@app.route('/opencast/upload')
def get_upload():
  message = "This is the Opencast master Endpoint."

  return message

@app.route('/opencast/acl')
def get_acl():
  message = "This is the Opencast acl Endpoint."

  return message

@app.route('/opencast/legacy')
def get_legacy():
  message = "This is the Opencast legacy Endpoint."

  return message


@app.route('/opencast/mllp', methods=['GET', 'POST'])
def get_mllp():
    event_id = request.args.get('event_id')
    txt = "This is the Opencast mllp Endpoint for the event_id: %s."%(event_id)
    filter = request.args.get('filter', default = '*', type = str)
    print(txt)
    return  {"data": command("mllp", "-ev_id=%s"%(event_id)),"error":None}


@app.route('/opencast/trs')
def get_trs():
  message = "This is the Opencast trs Endpoint."

  return message


@app.route('/opencast/new')
def get_new():
  message = "This is the Opencast legacy New."

  return message

if __name__ == '__main__':
    serve(app, host='0.0.0.0', port=8080)
